﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootStep_LoadGameDef : BootStep {
    
    [SerializeField]
    private GameDef GameDefPrefab;

    public override void Execute()
    {
        GameDef go = Instantiate(GameDefPrefab);
        go.name = "GameDef";
        base.Execute();
    }
}
