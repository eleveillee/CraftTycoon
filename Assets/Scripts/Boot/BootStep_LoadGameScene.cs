﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BootStep_LoadGameScene : BootStep
{
	public string SceneName;
	
	public override void Execute()
    {
        SceneManager.LoadScene(SceneName);
        //tartCoroutine(WaitAndLoadScene());
	}

    /*IEnumerator WaitAndLoadScene()
    {
        yield return new WaitForEndOfFrame();
        SceneManager.LoadScene(SceneName);
    }*/
}
