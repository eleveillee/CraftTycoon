﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootStep_SpawnWorld : BootStep {
    
    public override void Execute()
    {
        CityManager.Instance.Initialize();
        EventManager.Instance.Initialize();
        ArmyManager.Instance.Initialize();
    }
}
