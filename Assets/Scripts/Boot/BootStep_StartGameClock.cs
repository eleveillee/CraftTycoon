﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootStep_StartGameClock : BootStep {
    
    public override void Execute()
    {
        GameClock.Instance.Initialize();
    }
}
