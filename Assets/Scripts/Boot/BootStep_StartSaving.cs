﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootStep_StartSaving : BootStep {
    
    public override void Execute()
    {
        Profile.Instance.IsSaveAllowed = true;
    }
}
