﻿using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;

public class AssetGameDefinition : ScriptableObject
{  
    [Header("Datas")]
    public List<DataCity> Cities = new List<DataCity>();
    public List<DataItem> Items = new List<DataItem>();
    public List<DataBuilding> Buildings = new List<DataBuilding>();
    public List<DataJob> Jobs = new List<DataJob>();
    public List<DataResearch> Researches = new List<DataResearch>();
    public List<DataTech> Techs = new List<DataTech>();
    public List<DataUnit> Units = new List<DataUnit>();

    [Header("City")]
    public DataCity CapitalCity;
    public Building BuildingPrefab;
    public CityInstance DefaultCityInstance;
    public List<DataBuilding> BuildingsPriorityOrder = new List<DataBuilding>();

    [Header("Events")]
    [UnityEngine.Serialization.FormerlySerializedAs("EventsMainQuest")]
    public List<EventFight> MainEvents = new List<EventFight>();

    [Header("Quests")]
    public List<QuestInstance> MainQuests = new List<QuestInstance>();

    [Header("Other/Temp")]
    public Inventory NewWorkerCost;

    public DataCity GetCity(string id)
    {
        return Cities.Find(x => x.Id == id);
    }
    
    public DataItem GetItem(string id)
    {
        return Items.Find(x => x.Id == id);
    }

    public DataBuilding GetBuilding(string id)
    {
        return Buildings.Find(x => x.Id == id);
    }

    public DataScriptable GetAsset(string id)
    {
        DataCity city = Cities.Find(x => x.Id == id);
        DataItem item = Items.Find(x => x.Id == id);
        DataBuilding building = Buildings.Find(x => x.Id == id);
        if (city != null)
            return city;
        if (item != null)
            return item;
        if (building != null)
            return building;

        return null;
    }
    /*
    [Header("Data instances")]
    [SerializeField]
    private List<DataGoalScriptable> _goals;
    public List<DataGoalScriptable> Goals { get { return _goals; }}
    */
}

