﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class DataBuilding : DataScriptable
{
    public string Description;
    public List<DataFeature> Features = new List<DataFeature>();
    public Inventory Price = new Inventory();
    public Requirement Requirement;
    public DataJob Employee;
    public bool DoRequireEmployee = true;
}