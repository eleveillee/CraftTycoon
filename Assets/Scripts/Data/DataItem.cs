﻿

using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class DataItem : DataScriptable
{
    public Sprite Sprite;
    public float WorkRequired;
}
