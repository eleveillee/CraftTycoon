﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class DataJob : DataScriptable
{
    public float WorkPerSec;
    public string Description;
    public Requirement Requirement = new Requirement();
    public Inventory Price = new Inventory();
}

[Serializable]
public class JobInstance
{
    public JobInstance(DataJob data)
    {
        Data = data;
    }

    public float XP;
    public int Level = 1;
    public DataJob Data;
    public int Workers;

}