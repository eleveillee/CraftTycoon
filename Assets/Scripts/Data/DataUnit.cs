﻿using System;
using UnityEngine;
using System.Collections.Generic;


public class DataUnit : DataScriptable
{
    public int Power;
    public Inventory Price;
}