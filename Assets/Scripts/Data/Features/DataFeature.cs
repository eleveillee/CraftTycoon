﻿using System;
using UnityEngine;

[Serializable]
public class DataFeature : ScriptableObject
{
    public string Name;
}
