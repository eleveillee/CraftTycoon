﻿using System;
using UnityEngine;

[Serializable]
public class DataHouse : DataFeature
{
    public int Capacity;
}
