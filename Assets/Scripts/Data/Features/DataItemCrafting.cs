﻿using System;
using UnityEngine;

[Serializable]
public class DataItemCrafting : DataFeature
{
    public DataItem ItemInput;
    public DataItem ItemOutput;
}
