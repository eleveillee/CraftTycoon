﻿using System;
using UnityEngine;

[Serializable]
public class DataStorage : DataFeature
{
    public int Capacity;
}
