using Lean;
using UnityEngine;

// This script will zoom the main camera based on finger gestures
public class CameraController : MonoBehaviour
{
	[Tooltip("The minimum field of view angle we want to zoom to")]
	public float Minimum = 1.0f;
	
	[Tooltip("The maximum field of view angle we want to zoom to")]
	public float Maximum = 60.0f;

	public float moveSpeed = 300f; // Up for speed
	
	protected virtual void OnEnable()
	{
		LeanTouch.OnFingerDrag += OnFingerDrag;
	}

	protected virtual void OnDisable()
	{
		LeanTouch.OnFingerDrag -= OnFingerDrag;
	}

	void OnFingerDrag(LeanFinger fingers)
	{
		Move(fingers.DeltaScreenPosition);   
	}

	void Move(Vector2 deltaScreenPosition)
    {
        if (UIManager.Instance.CurrentPanel != null)
            return;

        deltaScreenPosition /= (moveSpeed / (GetComponent<Camera>().orthographicSize));
		transform.position = new Vector3(transform.position.x - deltaScreenPosition.x, transform.position.y - deltaScreenPosition.y , transform.position.z);
	}
	
	void LateUpdate()
	{
        if (UIManager.Instance.CurrentPanel != null)
            return;

		// Zoom
		if (Camera.main != null)
		{
			// Make sure the pinch scale is valid
			float input = Input.GetAxis("Mouse ScrollWheel") + 1; //Lean.LeanTouch.PinchScale
			//Debug.Log(input);
			if (Mathf.Abs(input) > 0.1f)
			{
				// Store the old size in a temp variable
				var orthographicSize = Camera.main.orthographicSize;

				// Scale the size based on the pinch scale
				orthographicSize /= input;
				
				// Clamp the size to out min/max values
				orthographicSize = Mathf.Clamp(orthographicSize, Minimum, Maximum);

				//Debug.Log(Camera.main.orthographicSize + " to " + orthographicSize);
				// Set the new size
				Camera.main.orthographicSize = orthographicSize;
			}
		}
	}
}