﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Currency
{
    [SerializeField]
    private int _gold;
    public int Gold { get { return _gold; } }

    [SerializeField]
    private int _gem;
    public int Gem { get { return _gem; } }

    public Action<Currency> OnUpdate;

    public Currency(int coin = 0, int gem = 0)
    {
        _gold = coin;
        _gem = gem;
    }

    public bool Has(Currency cost)
    {
        if (_gold < cost.Gold)
            return false;
        else if (_gem < cost.Gem)
            return false;

        return true;
    }

    public void Add(Currency addCurrency)
    {
        Add(addCurrency.Gold, addCurrency.Gem);
    }

    public void Remove(Currency addCurrency)
    {
        Add(-addCurrency.Gold, -addCurrency.Gem);
    }

    public bool TrySpend(Currency currency)
    {
        if (Has(currency))
        {
            Remove(currency);
            return true;
        }

        return false;
    }

    public bool TrySpend(int _gold, int _gem = 0)
    {
        return TrySpend(new Currency(_gold, _gem));
    }

    //Gold
    void AddCoin(int value)
    {
        SetCoin(_gold + value);
    }

    void SetCoin(int value)
    {
        _gold = Math.Max(value, 0);
    }

    //Gems
    void AddGem(int value)
    {
        SetGem(_gem + value);
    }

    void SetGem(int value)
    {
        _gem = Math.Max(value, 0);
    }
    
    public void Add(int coin, int gem = 0)
    {
        AddCoin(coin);
        AddGem(gem);

        if (OnUpdate != null)
            OnUpdate(this);

    }
}
