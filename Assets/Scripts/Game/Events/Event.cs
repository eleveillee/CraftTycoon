﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event {
    public string Name;
    public string Description;
    public bool IsCompleted;
    public Vector2 Position;
    public ConvoyInstance DockedConvoy { get { return Profile.Instance.Data.Convoys.Find(x => x.IsAtDestination && x.Destination == this); } }
}
