﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EventFight : Event {
    public Army Army;
    public Inventory Loot;

    public FightResult Result;

    public void Fight(Army playerArmy)
    {
        Result = playerArmy.Fight(Army);
        if(Result.DidPlayerWin)
        {
            DockedConvoy.Inventory.AddInventory(Loot);
            IsCompleted = true;
        }
    }
}
