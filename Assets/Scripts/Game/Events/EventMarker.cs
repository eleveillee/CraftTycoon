﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean;
using UnityEngine.UI;
using System;

public class EventMarker : MonoBehaviour {

    [SerializeField] Collider2D _collider2D;
    [SerializeField] TextMesh _textName;
    public EventFight EventFight;


    void OnEnable()
    {
        LeanTouch.OnFingerTap += OnFingerTap;
        //LeanTouch.OnFingerHeldDown += OnFingerHold;
    }

    void OnDisable()
    {
        LeanTouch.OnFingerTap -= OnFingerTap;
    }

    void OnFingerTap(LeanFinger finger)
    {
        if (_collider2D.IsTarget(finger) && UIManager.Instance.CurrentPanel == null)
            OnTap();
    }

    void OnTap()
    {
        UIManager.Instance.UIPanelEvent.Setup(Profile.Instance.Data.Capital.Instance.Army, this).Open();
    }

    public void Setup(EventFight eventfight)
    {
        EventFight = eventfight;
        _textName.text = EventFight.Name;
    }
}
