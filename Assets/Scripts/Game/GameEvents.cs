﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean;
using UnityEngine;
using UnityEngine.Serialization;

public static class GameEvents
{
    //City
    public static Action<City> OnCityTap;
    public static Action<Building, City> OnBuildingBuilt;
}

 
