﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class BuildingInstance
{
    public DataBuilding Data;
    public int Level;
    public BuildingState State = BuildingState.Empty;
    public Inventory Inventory;
    public List<FeatureInstance> Features = new List<FeatureInstance>();

    public DataCity DataCityRef;
    public CityInstance CityInstanceRef { get { return Profile.Instance.Data.CityInstances.Find(x => x.Data == DataCityRef);} }
    public JobInstance JobInstanceRef { get { return CityInstanceRef.Jobs.Find(x => x.Data = Data.Employee); } }

    //[NonSerialized]
    //private JobInstance _jobInstanceRef;
    //public JobInstance JobInstanceRef { get { if(_jobInstanceRef == null) _jobInstanceRef = CityInstanceRef.Jobs.Find(x => x.Data = Data.Employee); return _jobInstanceRef; } }

    public BuildingInstance(DataBuilding data, CityInstance cityInstance)
    {
        Debug.Log("BI.CONSTRUC");
        Data = data;
        CreateFeatureInstances();
        DataCityRef = cityInstance.Data;
    }
    
    void CreateFeatureInstances()
    {
        foreach(DataFeature data in Data.Features)
        {
            FeatureInstance fi;
            if(data is DataItemCrafting)
            {
                fi = (FeatureI_ItemCrafting)ScriptableObject.CreateInstance(typeof(FeatureI_ItemCrafting));
                fi.name = "ItemCrafting";
            }
            else
            {
                fi = (FeatureInstance)ScriptableObject.CreateInstance(typeof(FeatureInstance));
                fi.name = "Default";
            }
                

            fi.Setup(data, this);
            Features.Add(fi);
        }
    }

    public int GetNumberWorker()
    {
        return CityInstanceRef.Jobs.Find(x => x.Data == Data.Employee).Workers;
    }

    public Sprite GetVisual(City city)
    {
        if (Features.Count < 1)
            return null;

        return Features[0].GetVisual();
    }

    public float GetProgress(City city)
    {
        if (Features.Count < 1)
            return -1f;

        return Features[0].GetProgress();
    }

    public Building GetAssociatedBuilding(City city)
    {
        return city.Buildings.Find(x => x.Instance.Data == Data);
    }
}

public enum BuildingState
{
    Empty,
    Building,
    Built
}