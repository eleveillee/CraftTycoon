﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CityInstance
{
    public DataCity Data;
    public CityState State;

    #region Definition
    public int MaxWorkers;
    public Inventory Inventory = new Inventory();
    public List<BuildingInstance> Buildings = new List<BuildingInstance>();
    public List<DataTech> Techs = new List<DataTech>();
    public List<JobInstance> Jobs = new List<JobInstance>();
    public Army Army = new Army();
    #endregion

    #region HelperProperties
    public int EmployedWorkers { get { return Jobs.Sum(x => x.Workers); } }
    public int AvailableWorkers { get { return MaxWorkers - EmployedWorkers; } }
    public int AvailableHousing { get { return Buildings.FindAll(x => x.Level > 0).Sum(build => build.Data.Features.FindAll(feature => feature is DataHouse).Sum(feature => ((DataHouse)feature).Capacity) * build.Level); } }
    public bool IsUnlocked { get { return State == CityState.Unlocked; } }
    #endregion

    #region Setup
    public void Setup(DataCity data)
    {
        if (GameDef.Instance == null)
            return;

        Data = data;
        InitializeEmpty();
        CopyDefault();
    }
        
    public void InitializeEmpty()
    {
        Inventory = new Inventory();

        foreach (DataJob data in GameDef.Instance.Data.Jobs)
        {
            Jobs.Add(new JobInstance(data));
        }

        foreach (DataBuilding data in GameDef.Instance.Data.Buildings)
        {
            Buildings.Add(new BuildingInstance(data, this));
        }
    }

    public void CopyDefault()
    {
        CityInstance defaultInstance = GameDef.Instance.Data.DefaultCityInstance;
        MaxWorkers = defaultInstance.MaxWorkers;
        State = defaultInstance.State;

        #region Datas
        //Jobs
        if (GameDef.Instance.Data.Jobs != null)
        {
            foreach (JobInstance instance in defaultInstance.Jobs)
            {
                JobInstance curInstance = Jobs.Find(x => x.Data == instance.Data);
                if (curInstance != null) { curInstance.Workers = instance.Workers; }
            }
        }

        //Buildings
        if (defaultInstance.Buildings != null)
        {
            foreach (BuildingInstance instance in defaultInstance.Buildings)
            {
                BuildingInstance curInstance = Buildings.Find(x => x.Data == instance.Data);
                if (curInstance != null) { curInstance.Level = instance.Level; }
            }
        }

        // Items
        if (GameDef.Instance.Data.Items != null)
        {
            foreach (ItemInstance item in defaultInstance.Inventory.ItemList)
            {
                ItemInstance curItemQuantity = Inventory.ItemList.Find(x => x.Data == item.Data);
                if (curItemQuantity != null) { curItemQuantity.Quantity = item.Quantity; }
            }
        }

        // Units
        if (GameDef.Instance.Data.Units != null)
        {
            foreach (UnitInstance instance in defaultInstance.Army.Units)
            {
                UnitInstance curInstance = Army.Units.Find(x => x.Data == instance.Data);
                if (curInstance != null) { curInstance.Quantity = instance.Quantity; }
            }
        }

        #endregion
    }
    #endregion
}

public enum CityState
{
	Locked,
	Unlocked
}

//Todo: wipe this
public enum CityName
{
	Quebec,
	NewYork,
	California,
	BritishColombia,
	Manitoba,
	Seattle,
	Florida,
	Texas,
	Nebraska
}
	

 
