﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ConvoyInstance
{
    public Inventory Inventory;
    public Army Army;

    public float Speed = 0.1f; //
    public Event Destination;
    public CityInstance SourceInstance;
    public int TimeDeparture;
    public bool IsAtDestination;
    
    public City SourceCity { get { return CityManager.Instance.GetCity(SourceInstance); } }
    public Vector3 Path { get { return new Vector3(Destination.Position.x, Destination.Position.y) - SourceCity.transform.position; } }
    public float PathTime { get { return Path.magnitude / Speed; } }
    public float ElapsedSeconds { get { return (float)(new TimeSpan((int)DateTime.Now.Ticks - TimeDeparture).TotalSeconds); } }   
    public float PercentDone { get { return Mathf.Min(1f, (ElapsedSeconds / PathTime)); } }

    public void Dock()
    {
        IsAtDestination = true;
    }
}
    
 
