﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FeatureI_ItemCrafting : FeatureInstance
{
    private DataItemCrafting _data;
    bool _isDebugVerbose = false;

    // Use this for initialization
    protected override void OnSetup()
    {
        _data = (DataItemCrafting)_dataFeature;
    }

    // Update is called once per frame
    protected override void TickUpdate()
    {
        float workAdded = _buildingInstanceRef.GetNumberWorker();//ToDo : Fix this _buildingInstanceRef.JobInstanceRef.Level;
        WorkDone += workAdded;

        bool isWorkComplete = WorkDone >= _data.ItemOutput.WorkRequired;
        bool isInputAvailable = (_data.ItemInput == null || _buildingInstanceRef.CityInstanceRef.Inventory.TryRemoveItem(_data.ItemInput, _buildingInstanceRef.GetNumberWorker()));
        if (_isDebugVerbose) Debug.Log("[ItemCrafting] " + _data.Name + " - " + _data.ItemOutput + " = " + isWorkComplete + " & " + isInputAvailable);

        if (isWorkComplete && isInputAvailable)
            Craft();
    }

    void Craft()
    {
        _buildingInstanceRef.CityInstanceRef.Inventory.AddItem(_data.ItemOutput, _buildingInstanceRef.GetNumberWorker() * _buildingInstanceRef.Level);
        WorkDone -= _data.ItemOutput.WorkRequired;
    }

    public override float GetProgress()
    {
        if (_data == null || _buildingInstanceRef.GetNumberWorker() <= 0) // DataNull or no worker = 0f
            return 0f;

        if (_buildingInstanceRef.Inventory.IsFullOf(_data.ItemOutput)) //StorageFull = 1f;
            return 1f;

        //Debug.Log("Prod.Progress2 = " + (Time.time - m_timeLastCrafting) + " / " + _data.CoolDown + " = " + Mathf.Clamp((Time.time - _timeLastProduction) / _data.CoolDown, 0f, 1f));
        return Mathf.Clamp(WorkDone / _data.ItemOutput.WorkRequired, 0f, 1f);
    }

    public override Sprite GetVisual()
    {
        return _data == null ? null : _data.ItemOutput.Sprite;
    }
}


