﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class FeatureInstance : ScriptableObject
{
    public float WorkDone;
    
    [SerializeField]
    protected DataFeature _dataFeature;
    protected BuildingInstance _buildingInstanceRef;

    internal void Setup(DataFeature data, BuildingInstance buildingInstance)
    {
        _dataFeature = data;
        _buildingInstanceRef = buildingInstance;
        OnSetup();
    }

    protected virtual void OnSetup() { }

    protected virtual void TickUpdate()
    {

    }

    public void Tick()
    {
        if (!_buildingInstanceRef.Data.DoRequireEmployee || _buildingInstanceRef.GetNumberWorker() >= 1)
            TickUpdate();
    }

    public virtual Sprite GetVisual()
    {
        return null;
    }

    public virtual float GetProgress()
    {
        return -1f;
    }

}


