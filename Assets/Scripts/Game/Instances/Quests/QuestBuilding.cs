﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class QuestBuilding : QuestInstance
{
    public override QuestType Type { get{ return QuestType.Building; } }
    public DataBuilding Building;
}


