﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class QuestInstance : DataScriptable
{
    public abstract QuestType Type { get; }
}

public enum QuestType
{
    Building,
    Resource,
    Event
}


