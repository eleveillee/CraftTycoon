﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UnitInstance
{
    public UnitInstance(DataUnit data, int quantity = 0)
    {
        Data = data;
        Quantity = quantity;
    }

    public DataUnit Data;
    public int Quantity;
    //public int Level;

    public int Power { get { return Data.Power * Quantity; } }
}