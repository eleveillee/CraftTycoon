﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Inventory
{
    public Currency Currency = new Currency();

    public List<ItemInstance> ItemList = new List<ItemInstance>();
    //Dictionary<DataItem, int> ItemList = new Dictionary<DataItem, int>();
    public Action OnItemAdded;

    [NonSerialized]
    public int MaxSize = int.MaxValue;

    public Inventory()
    {
        if (GameDef.Instance == null)
            return;

        // Should I fill it with empty values or TryContains everytime ??? To Evaluate.
        foreach (DataItem item in GameDef.Instance.Data.Items)
        {
            ItemList.Add(new ItemInstance(item, 0));
        }
    }

    internal static Inventory Random()
    {
        Inventory inventory = new Inventory();
        foreach (DataItem data in GameDef.Instance.Data.Items)
        {
            inventory.ItemList.Add(new ItemInstance(data, UnityEngine.Random.Range(0, 20)));
        }
        return inventory;
    }

    public void AddItem(DataItem dataItem, int value = 1)
    {
        ItemInstance itemInstance = ItemList.Find(x => x.Data == dataItem);
        itemInstance.Quantity = Mathf.Clamp(itemInstance.Quantity + value, 0, MaxSize);
        OnItemAdded.SafeInvoke();
    }

    internal void AddItem(ItemInstance itemInstance)
    {
        AddItem(itemInstance.Data, itemInstance.Quantity);
    }

    public bool IsFullOf(DataItem dataItem)
    {
        return ItemList.Find(x => x.Data == dataItem).Quantity >= MaxSize;
    }

    internal void AddInventory(Inventory loot)
    {
        foreach (ItemInstance itemInstance in loot.ItemList)
        {
            AddItem(itemInstance);
        }
    }

    internal bool Transfert(Inventory inventory, DataItem dataItem, int itemQuantity)
    {
        if (TryRemoveItem(dataItem, itemQuantity))
        {
            inventory.AddItem(dataItem, itemQuantity);
            return true;
        }

        return false;
    }

    public bool TryRemoveItem(DataItem dataItem, int value = 1)
    {
        ItemInstance itemInstance = ItemList.Find(x => x.Data == dataItem);
        if (itemInstance.Quantity < value)
        {
            return false;
        }

        itemInstance.Quantity = Mathf.Clamp(itemInstance.Quantity - value, 0, MaxSize);
        return true;
    }

    public bool TryRemoveInventory(Inventory inventory)
    {
        if (!HasInventory(inventory))
            return false;

        foreach(ItemInstance quantity in inventory.ItemList)
        {
            TryRemoveItem(quantity.Data, quantity.Quantity);
        }
        return true;
    }

    public bool HasInventory(Inventory inventory)
    {
        foreach (ItemInstance quantity in inventory.ItemList)
        {
            if(!HasItem(quantity.Data, quantity.Quantity))
            {
                return false;
            }
        }

        return true;
    }

    public bool HasItem(DataItem dataItem, int value = 1)
    {
        ItemInstance itemQuantity = ItemList.Find(x => x.Data == dataItem);
        if (itemQuantity == null)
        {
            //Debug.LogWarning("TRYING TO DELETE AN INEXISTENT ITEM  id :" + _itemId);
            return false;
        }

        return itemQuantity.Quantity >= value;
    }

    /*public Dictionary<DataItem, int> ItemList
    {
        get { return ItemList; }
    }*/
}

[Serializable]
public class ItemInstance
{
    public ItemInstance(DataItem data, int quantity)
    {
        Data = data;
        Quantity = quantity;
    }

    public DataItem Data;
    public int Quantity;
}

/*
// Iron Plate
456a6ae3-30b7-4dfc-b79e-6fab85bfca71

// Gear 
dab1462a-551f-4678-84e3-60fb15705418

Iron : 
7d64d750-3d87-433d-ace0-74964bb8233d
*/
