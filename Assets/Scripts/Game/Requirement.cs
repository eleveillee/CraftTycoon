﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Requirement
{
    public List<DataBuilding> Buildings = new List<DataBuilding>();
    public List<DataResearch> Researches = new List<DataResearch>();
    public List<DataTech> Techs = new List<DataTech>();

    public bool IsCompleted(City city)
    {
        bool isCompleted = true;
        Buildings.ForEach((reqBuilding) => { if (city.Instance.Buildings.Find(cityBuilding => cityBuilding.Data == reqBuilding).Level < 1) isCompleted = false; });
        Researches.ForEach((reqResearch) => { if (Profile.Instance.Data.Researches.Find(cityResearch => cityResearch == reqResearch) == null) isCompleted = false; });
        Techs.ForEach((reqTech) => { if (city.Instance.Techs.Find(cityTech => cityTech == reqTech) == null) isCompleted = false; });
        //Debug.Log("IsCompleted : " + isCompleted);
        return isCompleted;
    }
}