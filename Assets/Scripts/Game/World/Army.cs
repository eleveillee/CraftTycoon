﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;



[Serializable]
public class Army
{
    // DATAS
    // Transport ?
    public List<UnitInstance> Units = new List<UnitInstance>();
    public String Name;

    // PROPERTIES
    public int Power { get { return Units.Sum(unitInstance => unitInstance.Power); } }
    public int NumberTypes { get { return Units.FindAll(unitInstance => unitInstance.Quantity > 0).Count; } }

    #region Code
    public Army()
    {
        if (GameDef.Instance == null)
            return;

        foreach (DataUnit unit in GameDef.Instance.Data.Units)
        {
            Units.Add(new UnitInstance(unit, 0));
        }
    }


    public void Clear()
    {
        foreach (UnitInstance instance in Units)
        {
            instance.Quantity = 0;
        }
    }

    public void RemovePower(int power)
    {
        foreach (UnitInstance unit in Units)
        {
            if (unit.Quantity <= 0)
                continue;

            int quantityRemove = (power / NumberTypes / unit.Data.Power);   
            unit.Quantity = Math.Max(unit.Quantity - quantityRemove, 0);
        }
    }

    public FightResult Fight(Army enemyArmy)
    {
        FightResult fightResult = new FightResult();

        int powerDiff = Power - enemyArmy.Power;
        fightResult.DidPlayerWin = powerDiff >= 0;
        if (powerDiff >= 0)
        {
            RemovePower(enemyArmy.Power);
            enemyArmy.Clear();
        }
        else
        {
            enemyArmy.RemovePower(Power);
            Clear();
        }
        
        return fightResult;
    }

    internal static Army Random()
    {
        Army army = new Army();
        foreach (DataUnit data in GameDef.Instance.Data.Units)
        {
            army.Units.Add(new UnitInstance(data, UnityEngine.Random.Range(0, 20)));
        }
        return army;
    }
    #endregion
}