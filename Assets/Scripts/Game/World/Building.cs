﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Lean;

[Serializable]
public class Building : MonoBehaviour
{
    public City City;

    public BuildingInstance Instance;
    //public List<Feature> Features = new List<Feature>();
    protected bool _isWorking;

    public Action<bool> OnBuildingToggled;
    
    public void Setup(BuildingInstance bInstance, City city)
    {
        City = city;
        Instance = bInstance;
        transform.name = "Building(" + bInstance.Data.Name + ")";
        //SpawnFeatures();
    }

    public void AddItem(DataItem dataItem, int value = 1)
    {
        City.Instance.Inventory.AddItem(dataItem, value);
    }

    /*public static void AddFeature(this Building building, GameObject go, DataFeature data)
    {
        if (data == null)
            return;

        Feature feature = null;
        if (data.GetType() == typeof(DataItemCrafting))
        {
            feature = go.AddComponent<ItemCrafting>();
        }

        if (feature != null)
        {
            feature.Setup(data);
            feature.Building = building;
            building.Features.Add(feature);
        }
    }*/
}