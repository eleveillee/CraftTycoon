﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean;
using UnityEngine;
using UnityEngine.Serialization;

public class City : MonoBehaviour
{
	public DataCity Data;
	public List<City> ConnectedCities = new List<City>(); //ToDelete
    public List<Building> Buildings = new List<Building>();

	private Collider2D _collider2D;
	public CityInstance Instance;

    public Inventory Inventory { get { return Instance.Inventory; } }

    void Awake()
	{
		_collider2D = GetComponentInChildren<Collider2D>();
        Instance = Profile.Instance.Data.CityInstances.Find(x => x.Data == Data);
        Refresh();
    }

    private void Start()
    {
        SpawnBuildings();
        UpdateMaxSize();
    }

    public void UpdateMaxSize()
    {
        int maxSize = 0;
        foreach (BuildingInstance instance in Instance.Buildings)
        {
            foreach (DataFeature feature in instance.Data.Features)
            {
                if(feature is DataStorage)
                {
                    maxSize += ((DataStorage)feature).Capacity;
                }
            }
        }

        Instance.Inventory.MaxSize = maxSize;
    }

    public void Tick()
    {
        foreach(BuildingInstance buildingInstance in Instance.Buildings)
        {
            if (buildingInstance.Level <= 0)
                continue;

            foreach(FeatureInstance featureInstance in buildingInstance.Features)
            {
                featureInstance.Tick();
            }
        }
    }

    public void Refresh()
    {
        ChangeState(Instance.State);
    }

	void ChangeState(CityState state)
	{
		if (state == CityState.Locked)
		{
			GetComponentInChildren<SpriteRenderer>().color = Color.gray;
		}
		else
		{
			GetComponentInChildren<SpriteRenderer>().color = Color.white;
		}
	}
	
	protected virtual void OnEnable()
	{
		LeanTouch.OnFingerTap += OnFingerTap;
		//LeanTouch.OnFingerHeldDown += OnFingerHold;
	}

	protected virtual void OnDisable()
	{
		LeanTouch.OnFingerTap -= OnFingerTap;
	}

	/*void OnFingerHold(LeanFinger finger)
	{

		if ((OnTap != null) && _collider2D.IsTarget(finger))
			OnHold();
	}*/
	
	void OnFingerTap(LeanFinger finger)
	{
		if (_collider2D.IsTarget(finger))
			OnTap();
	}
    	
	void OnTap()
	{
        if(UIManager.Instance.CurrentPanel == null && !ArmyManager.Instance.IsSelecting)
            UIManager.Instance.UIPanelCity.Setup(this).Open(true);  

        if (GameEvents.OnCityTap != null)
            GameEvents.OnCityTap.Invoke(this);

    }

    internal void BuildNew(BuildingInstance bInstance)
    {
       if(bInstance.Level > 0)
            return;

        bInstance.State = BuildingState.Built;
        bInstance.Level = 1;
        Building newBuilding = SpawnBuilding(bInstance);
        GameEvents.OnBuildingBuilt.SafeInvoke(newBuilding, this);
    }

    void SpawnBuildings()
    {
        if (Instance == null)
            return;

        foreach (BuildingInstance bInstance in Instance.Buildings)
        {
            if(bInstance.Level > 0)
                SpawnBuilding(bInstance);
        }
    }

    public Building SpawnBuilding(BuildingInstance bInstance)
    {
        if (Buildings.Find(x => x.Instance == bInstance) != null)
            return null;

        Building building = Instantiate(GameDef.Instance.Data.BuildingPrefab, Vector3.zero, Quaternion.identity);
        building.Setup(bInstance, this);
        Buildings.Add(building);
        building.transform.parent = transform;
        UpdateMaxSize();

        return building;
    }

    void OnDrawGizmosSelected()
	{
		ConnectedCities.ForEach((city) => { if (city.gameObject.activeInHierarchy) Gizmos.DrawLine(transform.position, city.transform.position);  } );
	}
}