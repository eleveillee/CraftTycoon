﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean;
using UnityEngine;
using UnityEngine.Serialization;

public class Convoy : MonoBehaviour
{	
	private Collider2D _collider2D;
	public ConvoyInstance Instance;

    public void Setup(ConvoyInstance convoyInstance)
    {
        _collider2D = GetComponentInChildren<Collider2D>();
        Instance = convoyInstance;
        UpdateTransform();
    }
	
	protected virtual void OnEnable()
	{
		LeanTouch.OnFingerTap += OnFingerTap;
	}

	protected virtual void OnDisable()
	{
		LeanTouch.OnFingerTap -= OnFingerTap;
	}
    	
	void OnFingerTap(LeanFinger finger)
	{
		if (_collider2D.IsTarget(finger))
			OnTap();
	}

    private void Update()
    {
        if (Instance.IsAtDestination)
            return;

        UpdateTransform();
    }

    void UpdateTransform()
    {
        transform.position = Instance.SourceCity.transform.position + Instance.Path * Instance.PercentDone;
        transform.rotation = Quaternion.Euler(0f, 0f, transform.position.AngleWith(Instance.Destination.Position));

        if (Instance.PercentDone >= 1f && !Instance.IsAtDestination)
        {
            transform.position = Instance.Destination.Position;
            Instance.Dock();
            return;
        }
    }

    void OnTap()
	{
        Debug.Log("Convoy.Tap");
    }

    /*void OnDrawGizmosSelected()
	{
		ConnectedCities.ForEach((city) => { if (city.gameObject.activeInHierarchy) Gizmos.DrawLine(transform.position, city.transform.position);  } );
	}*/
}