﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public static class HelperEditorTools
{
    static AssetGameDefinition _gameDefinition;

    [MenuItem("Tools/Delete PlayerPrefs")]
    public static void DeleteAllPlayerPrefs()
    {
        Debug.Log("[Helper] Player prefs deleted");
        PlayerPrefs.DeleteAll();
    }

    // This is called by the editor menu.
    [MenuItem("Tools/Update GameDefinition")]
    public static void UpdateGameDefinition()
    {
        //_gameDefinition = Resources.Load("Data/GameDefinition", typeof(AssetGameDefinition)) as AssetGameDefinition;

        _gameDefinition = Resources.FindObjectsOfTypeAll<AssetGameDefinition>()[0];
        if (_gameDefinition == null)
            Debug.Log("Null GameDef");

        _gameDefinition.Buildings = FindAssetOfType<DataBuilding>();
        _gameDefinition.Cities = FindAssetOfType<DataCity>();
        _gameDefinition.Items = FindAssetOfType<DataItem>();
        _gameDefinition.Jobs = FindAssetOfType<DataJob>();
        _gameDefinition.Researches = FindAssetOfType<DataResearch>();
        _gameDefinition.Techs = FindAssetOfType<DataTech>();
        _gameDefinition.Units = FindAssetOfType<DataUnit>();

        EditorUtility.SetDirty(_gameDefinition);
    }

    public static List<T> FindAssetOfType<T>() where T : UnityEngine.Object
    {
        List<T> assets = new List<T>();
        //string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
        string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T).ToString().Replace("UnityEngine.", "")));

        for (int i = 0; i < guids.Length; i++)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
            T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
            if (asset != null)
            {
                assets.Add(asset);
            }
        }
        return assets;
    }
}
