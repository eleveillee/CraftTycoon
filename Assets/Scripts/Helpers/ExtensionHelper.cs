﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;

using Random = UnityEngine.Random;

public static class ExtensionHelper {

    // Actions
    public static void SafeInvoke(this Action action)
    {
        if (action != null)
            action.Invoke();
    }
    
    public static void SafeInvoke<T1>(this Action<T1> action, T1 t1)
    {
        if (action != null)
            action.Invoke(t1);
    }

    public static void SafeInvoke<T1, T2>(this Action<T1, T2> action, T1 t1, T2 t2)
    {
        if (action != null)
            action.Invoke(t1, t2);
    }

    public static void SafeInvoke<T1, T2, T3>(this Action<T1, T2, T3> action, T1 t1, T2 t2, T3 t3)
    {
        if (action != null)
            action.Invoke(t1, t2, t3);
    }

    // Vector2
    public static float GetRandomValue(this Vector2 vector)
    {
        return Random.Range(vector.x, vector.y);
    }
    
    // Vector3
    public static float AngleWith(this Vector3 vectorFrom, Vector3 vectorTo)
    {
        Vector3 diff =  vectorTo - vectorFrom;
        diff.z = 0f;
        diff.Normalize();

        return Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
    }
    

    // Text
    public static void SetValue(this UnityEngine.UI.Text text, float value, float duration = 0.0f)
    {
        text.text = String.Format("{0:0}", value);
    }

    // List
    public static T GetRandomValue<T>(this List<T> list)
    {
        return list[Random.Range(0, list.Count)];
    }
         
    public static void MoveItemAtIndexToFront<T>(this List<T> list, int index)
    {
        T item = list[index];
        for (int i = index; i > 0; i--)
            list[i] = list[i - 1];
        list[0] = item;
    }
    public static List<T> Clone<T>(this List<T> list)
    {
        return list.GetRange(0, list.Count);
    }
    
    // Transform
    public static void SetY(this Transform transform, float value)
    {
        transform.localPosition = new Vector3(transform.localPosition.x, value, transform.localPosition.z);
    }

    // GameObject
    public static void ToggleActive(this GameObject go)
    {
        go.SetActive(!go.activeSelf);
    }
    
    // Collider2D
    public static bool IsTarget(this Collider2D collider2D, Lean.LeanFinger finger)
    {
        Vector3 wp = Camera.main.ScreenToWorldPoint(finger.ScreenPosition);
        Vector2 touchPos = new Vector2(wp.x, wp.y);

        if (collider2D == null)
            return false;

        return collider2D.bounds.Contains(touchPos);


    }
    
}
