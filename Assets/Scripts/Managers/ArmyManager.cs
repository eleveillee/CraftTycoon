﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class ArmyManager : MonoBehaviour
{
    [SerializeField] Transform _convoysRoot;
    [SerializeField] Convoy _prefabConvoy;

    public static ArmyManager Instance;

    [NonSerialized]
    public bool IsSelecting;

    EventMarker _currentSelectedMarker;
    List<Convoy> Convoys = new List<Convoy>();

    // Use this for initialization
    void Awake ()
	{
	    if (Instance == null)
	        Instance = this;
        else
            Destroy(gameObject);
    }

    public void Initialize()
    {
        SpawnAllConvoys();
    }

    public void PrepareArmyTowardsEvent(EventMarker marker)
    {
        _currentSelectedMarker = marker;
        SelectArmySource();
        // Open UIArmy, Split Army, Confirm
        // Launch new army
    }

    void SelectArmySource()
    {
        if(Profile.Instance.Data.CityInstances.FindAll(x => x.IsUnlocked).Count == 1)
        {
            OnCityTap(Profile.Instance.Data.Capital);
        }
        else
        {
            ToggleSelection(true);
        }
    }

    void ToggleSelection(bool isSelecting)
    {
        IsSelecting = isSelecting;
        if(isSelecting)
            GameEvents.OnCityTap += OnCityTap;
        else
            GameEvents.OnCityTap -= OnCityTap;
    }

    private void OnCityTap(City city)
    {
        UIManager.Instance.UIPanelArmy.Setup(_currentSelectedMarker, city).Open();
        ToggleSelection(false);
        _currentSelectedMarker = null;
    }
    
    public void LaunchConvoy(City sourceCity, EventMarker targetMarker, Army army)
    {
        // ConvoyInstance
        ConvoyInstance convoyInstance = new ConvoyInstance();
        convoyInstance.SourceInstance = sourceCity.Instance;
        convoyInstance.Army = army;
        convoyInstance.TimeDeparture = (int)DateTime.Now.Ticks;
        convoyInstance.Destination = targetMarker.EventFight;

        Profile.Instance.Data.Convoys.Add(convoyInstance);

        // Convoy
        SpawnConvoy(convoyInstance, sourceCity.transform.position);
    }

    public void SpawnConvoy(ConvoyInstance convoyInstance, Vector3 position)
    {
        position.z = -5f;
        Convoy convoy = Instantiate(_prefabConvoy, position, Quaternion.identity, _convoysRoot.transform);
        convoy.Setup(convoyInstance);
        Convoys.Add(convoy);
    }

    public void SpawnAllConvoys()
    {
        foreach(ConvoyInstance convoyInstance in Profile.Instance.Data.Convoys)
        {
            City city = CityManager.Instance.GetCity(convoyInstance.SourceInstance);
            SpawnConvoy(convoyInstance, city.transform.position);
        }
    }
}