using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class BuildingManager
{
    /*
    [SerializeField]
    private GameObject _buildingPrefab;

    private List<Building> _buildings = new List<Building>();
    public Action<Building> OnBuildingLevelUp;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    void Start()
    {
        SpawnStartingBuildings(); 
    }

    // To decide which builidng are created at start, most are temporary
    private void SpawnStartingBuildings()
    {
        SpawnBuilding("f23f4949-6ac6-4e41-8258-e87f54423a76", new Vector2(750f, 0f)); // portal
        SpawnBuilding("0d9ff657-886a-412a-9b37-3b1b8b69ea29", new Vector2(-750f, 0f)); // Battery
        //SpawnBuilding("781c6e7e-0530-4cf8-8ffd-6c0cfa68cf68", 750f * Vector2.up); // Anteanna
        SpawnBuilding("a8369280-c249-4214-a016-937b8d2d0658", 750f * Vector2.up); // Quarry
        SpawnBuilding("53586b0b-f430-4d23-95d4-563621ddce58", 750f * Vector2.down); // Factory
        SpawnBuilding("84a6921a-25a9-4ad5-b597-9e91c1ffc16a", 750f * Vector2.up + 750f * Vector2.left); // Furnace
    }

    void SpawnBuilding(string buildingId, Vector2 position)
    {
        DataBuilding data = GameManager.Instance.GameDefinition.GetbuildingById(buildingId);
        if (data == null)
        {
            Debug.LogWarning("Building data missing for : " + buildingId);
            return;
        }



        Building building = ((GameObject) Instantiate(_buildingPrefab, position, Quaternion.identity)).GetComponent<Building>();
        building.Setup(data);
        _buildings.Add(building); 
    }

    public Building GetRandomBuilding()
    {
        return _buildings[UnityEngine.Random.Range(0,_buildings.Count)];
    }

    public Building GetBuilding(string buildingId)
    {
        return _buildings.Find(x => x.Data.Id == buildingId);
    }

    public List<Building> Buildings
    {
        get { return _buildings; }
        set { _buildings = value; }
    }
    */
}
