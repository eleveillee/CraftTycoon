﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Lean;
using UnityEngine;

public class CityManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _linePrefab;
    
    [SerializeField]
    private Transform _linesRoot;

    public List<City> Cities;
    
    public static CityManager Instance;

    void Awake()
    {
        Instance = this;
        Cities = FindObjectsOfType<City>().ToList();
    }

    public void Initialize()
    {
        //ConnectLines();    
    }
    
    public City GetCity(CityInstance cityInstance)
    {
        return Cities.Find(x => x.Instance.Data == cityInstance.Data);
    }
    
    public void Tick()
    {
        foreach(City city in Cities)
        {
            city.Tick();
        }
    }

    /*void ConnectLines()
    {
        foreach(CityInstance instance in Profile.Instance.Data.CityInstances)
        {
            if (!instance.IsUnlocked)
                continue;

            foreach (var connectedCityName in instance.Data.ConnectedCities)
            {
                City city = GetCity(instance.Data);
                City connectedCity = GetCity(connectedCityName);

                if(connectedCity == null || !connectedCity.Instance.IsUnlocked || city == null)
                    continue;

                GameObject _lineGO = Instantiate(_linePrefab) as GameObject;
                _lineGO.transform.SetParent(_linesRoot);

                Vector3 position = (connectedCity.transform.position + city.transform.position) / 2f;
                position.z = 1f;
                _lineGO.transform.position = position;

                //float angle = Vector2.Angle(connectedCity.transform.position, city.transform.position);
                //Debug.Log(connectedCity.name + " to " + city.name + " = " + angle);
                _lineGO.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, connectedCity.transform.position.AngleWith(city.transform.position)));

                float distance = Vector3.Distance(connectedCity.transform.position, city.transform.position);
                _lineGO.transform.localScale = new Vector3(distance * 100f, 8f, 0f);
            }
        }
    }*/



    /*public int GetCityPrice(CityName name)
    {
        int distancePrice = (int)(Vector3.Distance(GetCity(name).transform.position, Profile.Instance.Data.Capital.transform.position)*10) * 5;

        return distancePrice;
    }*/

    /*public void RefreshCities()
    {
        Cities.ForEach(city => city.Refresh());

        foreach(Transform t in _linesRoot)
        {
            Destroy(t.gameObject);
        }
        ConnectLines();

    }*/
}


