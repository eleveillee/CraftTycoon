using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Random = UnityEngine.Random;

public class EventManager : MonoBehaviour {

    [SerializeField] EventMarker _eventMarkerPrefab;
    [SerializeField] Transform _eventsRoot;

    List<EventMarker> _markers = new List<EventMarker>();

    public static EventManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void Initialize()
    {
        SpawnProfileEvents();
        if (string.IsNullOrEmpty(Profile.Instance.Data.MainEvent.Name))
        {
            SpawnNextMainQuest();
        }
    }

    void SpawnNextMainQuest()
    {
        int mainQuestIndex = GameDef.Instance.Data.MainEvents.FindIndex(x => x.Name == Profile.Instance.Data.MainEvent.Name);
        
        EventFight newEventFight = GameDef.Instance.Data.MainEvents[mainQuestIndex + 1];
        SpawnEvent(newEventFight);
        Profile.Instance.Data.MainEvent = newEventFight;
        Profile.Instance.Data.Events.Add(newEventFight);
    }

    void SpawnProfileEvents()
    {
        foreach (EventFight eventFight in Profile.Instance.Data.Events)
        {
            SpawnEvent(eventFight);
        }
    }

    /*private void CreateRandomEvent()
    {
        Vector3 dirVector = Random.insideUnitCircle.normalized * Random.Range(0.75f, 1.75f);
        Vector3 randomPosition = Profile.Instance.Data.Capital.transform.position + dirVector;

        EventFight eventFight = new EventFight();
        eventFight.Name = "Camp1";
        eventFight.Description = "Fight to advance in the main quest!";
        eventFight.Army = Army.Random();
        eventFight.Loot = Inventory.Random();
        eventFight.Position = randomPosition;
        Profile.Instance.Data.Events.Add(eventFight);

        SpawnEvent(eventFight);
    }*/

    private void SpawnEvent(EventFight eventFight)
    {
        EventMarker eventMarker = Instantiate(_eventMarkerPrefab, _eventsRoot);
        eventMarker.Setup(eventFight);
        eventMarker.transform.position = eventFight.Position;
        _markers.Add(eventMarker);
    }

    public void ClearEvent(EventFight eventFight)
    {
        EventMarker marker = _markers.Find(x => x.EventFight == eventFight);
        _markers.Remove(marker);
        Destroy(marker.gameObject);
    }

    public EventMarker GetEvent(Event thisEvent)
    {
        return _markers.Find(x => x.EventFight == thisEvent);
    }
}   