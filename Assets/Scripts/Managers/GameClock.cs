﻿using UnityEngine;
using System.Collections;
using System;

public class GameClock : MonoBehaviour
{
    public static GameClock Instance;
    bool _isDebugVerbose = false;
    public static float Frequency = 0.1f;
    public static int LimitTicks = 10000;

    private void Awake()
    {
        Instance = this;
    }

    public void Initialize()
    {
        ProcessOfflineTime();
        StartCoroutine(Tick_CR());
    }

    void ProcessOfflineTime()
    {
        if (Profile.Instance.Data.TimeSave <= 0)
            return;

        TimeSpan ts = new TimeSpan(DateTime.Now.Ticks - Profile.Instance.Data.TimeSave);
        //Debug.Log(DateTime.Now.Ticks + " - " + Profile.Instance.Data.TimeSave + " = " + (DateTime.Now.Ticks + " - " + Profile.Instance.Data.TimeSave));

        if (_isDebugVerbose) Debug.Log("[GameClock] ProcessOfflineStart : " + (int)ts.TotalSeconds + " seconds elapsed");
        float timePreOff = Time.time;
        long neededTicks = (long)(ts.TotalSeconds / Frequency);
        if (neededTicks > LimitTicks)
        {
            Debug.Log("[GameClock] Too Much Ticks neede " + neededTicks + " / " + LimitTicks);
            neededTicks = LimitTicks;
        }

        for (int i = 0; i < neededTicks; i++)
        {
            Tick();
        }
        if (_isDebugVerbose) Debug.Log("[GameClock] ProcesedOffline " + neededTicks + " ticks in " + (Time.time - timePreOff) + " seconds");
    }

    void Tick()
    {
        //Debug.Log("Tick");
        CityManager.Instance.Tick();
    }

    IEnumerator Tick_CR()
    {
        Tick();
        yield return new WaitForSeconds(Frequency);
        StartCoroutine(Tick_CR());
    }    
}