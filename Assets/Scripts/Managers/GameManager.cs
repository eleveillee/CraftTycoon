﻿using UnityEngine;
using System.Collections;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    // Use this for initialization
    void Awake ()
	{
	    if (Instance == null)
	        Instance = this;
        else
            Destroy(gameObject);
        
        LeanTween.init(400);
    }
    
}