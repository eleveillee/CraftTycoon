﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour {

    public static QuestManager Instance;
    //static bool _isDebugVerbose = false;

    public Action OnNewQuest;

    void Awake () {
        Instance = this;
        ConnectDelegates();
	}

    void ConnectDelegates()
    {
        GameEvents.OnBuildingBuilt += OnBuildingBuilt;
    }

    private void OnBuildingBuilt(Building building, City city)
    {
        if(Profile.Instance.Data.MainQuest is QuestBuilding && ((QuestBuilding)Profile.Instance.Data.MainQuest).Building == building.Instance.Data)
        {
            StartNextQuest();
        }
    }

    void StartNextQuest()
    {
        int index = GameDef.Instance.Data.MainQuests.FindIndex(x => x == Profile.Instance.Data.MainQuest);
        if(GameDef.Instance.Data.MainQuests.Count < index)
            Profile.Instance.Data.MainQuest = GameDef.Instance.Data.MainQuests[index + 1];

        OnNewQuest.SafeInvoke();
    }


    // Find the current Main Quest, if null use the first one. Otherwise get the next one.
    /*public static QuestInstance GetNextMainQuest()
    {
        QuestInstance _currentMainQuest = Profile.instance.Data.Quests.Find(x => x.Data.Source == QuestSource.Main);
        if (_currentMainQuest == null)
            return new QuestInstance(GameManager.Instance.GameDefinition.Quests[0].Data);

        int index = GameManager.Instance.GameDefinition.Quests.FindIndex(x => x.Data.Id == _currentMainQuest.Data.Id);
        if (index >= GameManager.Instance.GameDefinition.Quests.Count - 1)
            return null;

        return new QuestInstance(GameManager.Instance.GameDefinition.Quests[index + 1].Data);
    }*/

    /*public static Quest GetRandomQuest()
    {
        Array enumQuestTypes = Enum.GetValues(typeof(QuestType));
        QuestType type = ((QuestType)enumQuestTypes.GetValue(Random.Range(0, enumQuestTypes.Length)));

        Array enumQuestDuration = Enum.GetValues(typeof(QuestDuration));
        QuestDuration duration = ((QuestDuration)enumQuestDuration.GetValue(Random.Range(0, enumQuestDuration.Length)));

        // Speed can't be cumulative. Better system need to be in place, dictonnary of forbidden quest ?
        if (type == QuestType.Speed)
        {
            while (duration == QuestDuration.Cumul)
            {
                duration = ((QuestDuration)enumQuestDuration.GetValue(Random.Range(0, enumQuestDuration.Length)));
            }
        }

        List<string> values = new List<string>() { "10" }; // Objectif, to put parametrized values
        Currency rewardCurrency = new Currency(Random.Range(2, 10));
        DataQuest newDataQuest = new DataQuest(type, duration, QuestSource.Random, rewardCurrency, values);
        Quest newQuest = new Quest(newDataQuest);
        if (_isDebugVerbose) Debug.Log("[QuestSystem] NewQuest : " + newQuest.Data.Type + " - " + newQuest.Data.Values[0]);

        return newQuest;
    }*/

    /*public static void CompleteQuest(QuestInstance completedQuest)
    {
        if (_isDebugVerbose) Debug.Log("[QuestSystem] Completed : " + completedQuest.Data.Type);
        GameManager.Instance.PlayerProfile.Inventory.Currency.Add(completedQuest.Data.RewardCurrency);
        if (completedQuest.Data.Duration == QuestDuration.Cumul)
            ResetCumulativeData(completedQuest.Data.Type);

        GameManager.Instance.PlayerProfile.Quests.Remove(completedQuest);
        UpdateNewQuest(completedQuest);
    }

    static void UpdateNewQuest(QuestInstance lastCompletedQuest)
    {
        QuestInstance newQuest;
        if (lastCompletedQuest.Data.Source == QuestSource.Main)
            newQuest = GetNextMainQuest();
        else
            newQuest = GetRandomQuest();

        if (newQuest == null)
        {
            Debug.LogWarning("Last Main Quest not properly handled yet");
            return;
        }

        GameManager.Instance.PlayerProfile.AddQuest(newQuest);
        OnNewQuest.SafeInvoke();
    }

    static void ResetCumulativeData(QuestType type)
    {
        TrackingData data = GameManager.Instance.PlayerProfile.CumulativeTrackingData;
        switch (type)
        {
            case QuestType.Height:
                data.MaxHeight = 0;
                break;
            case QuestType.Gold:
                data.CurGold = 0;
                break;
            case QuestType.Speed:
                data.MaxSpeed = 0;
                break;
            case QuestType.Time:
                data.TimeLength = 0;
                break;
            case QuestType.Score:
                data.Score = 0;
                break;
            default:
                Debug.LogWarning("[QuestSystem] Unknown quest type when reinitializing cumulative value");
                break;
        }
    }

    static void InitializeQuests()
    {
        foreach (QuestInstance quest in GameManager.Instance.PlayerProfile.Quests)
        {
            quest.Initialize();
        }
    }*/

}