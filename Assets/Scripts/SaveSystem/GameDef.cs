using UnityEngine;
using System;

[Serializable]
public class GameDef : MonoBehaviour
{
    public static GameDef Instance;
    
    public AssetGameDefinition Data;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }
}