using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class Profile : MonoBehaviour
{
    public const float CURRENT_VERSION = 0.1f;
    public static Profile Instance;
    private bool _isDebugVerbose = false;
    private string _playerPrefName = "Save";
    public ProfileData Data = null;

    public float SaveCooldown = 1f;
    public bool IsSaveAllowed = false;
    //public static bool IsTutorial = true; // Hack To trigger tutorial on new profile
    public bool IsAlwaysNewProfile; // Hack to always create new profile

    //public Action<PlayerProfile> OnUpdate;
    public void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }

    public void CreateOrLoadProfile()
    {
        if (!IsAlwaysNewProfile && PlayerPrefs.HasKey(_playerPrefName))
        {
            Data = Load();
            if (_isDebugVerbose) Debug.Log("[Profile] LoadProfile Version" + Data.Version);
        }
        else
        {
            Data = new ProfileData();
            Data.Initialize();
            if (_isDebugVerbose) Debug.Log("[Profile] New Profile");
        }

        StartCoroutine(Save_CR());
    }

    private IEnumerator Save_CR()
    {
        Save();
        yield return new WaitForSeconds(SaveCooldown);
        StartCoroutine(Save_CR());
    }

    #region Save
    public void Save()
    {
        if (!IsSaveAllowed)
            return;

        //Debug.Log("Save: " + DateTime.Now);
        Data.TimeSave = DateTime.Now.Ticks;

        string saveJson = Serialize();
        PlayerPrefs.SetString(_playerPrefName, saveJson);
        if (_isDebugVerbose) Debug.Log("[SaveSystem] Save to Json : " + saveJson);
    }

    public string Serialize()
    {
        return JsonUtility.ToJson(Data);
    }
    #endregion

    #region Load
    public ProfileData Load()
    {
        string jsonSave = PlayerPrefs.GetString(_playerPrefName);
        if (_isDebugVerbose) Debug.Log("[SaveSystem] Load from Json : " + jsonSave);
        return Deserialize(jsonSave);
    }

    public ProfileData Deserialize(string jsonSave)
    {
        return JsonUtility.FromJson<ProfileData>(jsonSave);
    }
    #endregion

    public void DeleteProfile()
    {
        if (_isDebugVerbose) Debug.Log("[SaveSystem] Save Deleted and profile restarted");
        PlayerPrefs.DeleteAll();
    }
    /*
    public static void AllowSave(bool isAllowed)
    {
        _isSaveAllowed = isAllowed;
    }*/
}