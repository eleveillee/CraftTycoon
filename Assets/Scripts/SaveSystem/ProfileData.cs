using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class ProfileData
{
    [SerializeField]
    private float _version;
    public float Version { get { return _version; } }
    public long TimeSave;

    //Definitions
    public Inventory Inventory;
    public List<DataResearch> Researches = new List<DataResearch>();
    public List<CityInstance> CityInstances = new List<CityInstance>();
    public List<ConvoyInstance> Convoys = new List<ConvoyInstance>();

    // Events
    public EventFight MainEvent;
    public List<EventFight> Events = new List<EventFight>();

    // Quests
    public QuestInstance MainQuest;
    public List<QuestInstance> Quests = new List<QuestInstance>();
    
    public BuildingInstance CurrentBuilding = null; // Should be in Feature_BuilderInstance
    public City Capital { get { return CityManager.Instance.Cities.Find(x => x.Instance.Data == GameDef.Instance.Data.CapitalCity); } }

    public void Initialize() //Todo: Use a BaseProfileData from GameDef like BaseCity
    {
        _version = Profile.CURRENT_VERSION;
        Inventory = new Inventory();

        foreach (DataCity dataCity in GameDef.Instance.Data.Cities)
        {
            CityInstance cityInstance = new CityInstance();
            cityInstance.Setup(dataCity);
            CityInstances.Add(cityInstance);

            if (cityInstance.Data == GameDef.Instance.Data.CapitalCity)
                cityInstance.State = CityState.Unlocked;
        }

        MainQuest = GameDef.Instance.Data.MainQuests[0];
    }



}