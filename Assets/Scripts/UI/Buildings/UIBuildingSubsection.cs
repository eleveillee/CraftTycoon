﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBuildingSubsection : MonoBehaviour
{
    [Header("Common")]
    [SerializeField] Text _uiBuildingHeader;
    [SerializeField] Text _levelText;
    [SerializeField] Button _upgradeButton;

    [Header("UIBuildings")]
    [SerializeField] List<UIBuildingElement> UIBuildings;
    UIBuilding _currentUIBuilding;
    BuildingInstance _currentBInstance;
    City _city;

    public void Setup(BuildingInstance currentBuilding, City city)
    {
        _city = city;
        _currentBInstance = currentBuilding;
        foreach (UIBuildingElement uiBuildingElement in UIBuildings) {uiBuildingElement.UIBuilding.gameObject.SetActive(false);}

        UIBuildingElement element = UIBuildings.Find(x => x.Data == currentBuilding.Data);
        if (element != null)
        {
            _currentUIBuilding = element.UIBuilding;
            _currentUIBuilding.Setup(currentBuilding, _city);
            _currentUIBuilding.gameObject.SetActive(true);
        }
        Refresh();
    }

    public void Refresh()
    {
        if (_currentBInstance == null)
            return;

        _uiBuildingHeader.text = _currentBInstance.Data.Name;

        if(_currentBInstance.Level == 0)
        {
            _levelText.gameObject.SetActive(false);
            _upgradeButton.GetComponentInChildren<Text>().text = "BUILD";
            //_levelText.text = "LEVEL " + _currentBInstance.Level
        }
        else
        {
            _levelText.gameObject.SetActive(true);
            _levelText.text = "LEVEL " + _currentBInstance.Level;
            _upgradeButton.GetComponentInChildren<Text>().text = "UPGRADE";
        }

        if (_currentUIBuilding != null)
        {
            //Debug.Log("CurrentBuild : " + _currentUIBuilding.gameObject.name + " = " + _currentUIBuilding.gameObject.activeSelf + " = " + _currentUIBuilding.gameObject.activeInHierarchy);
            _currentUIBuilding.Refresh();
        }
            
    }

    public void zClick_UpgradeBuilding()
    {
        
        if (_currentBInstance.Level == 0) // Build
        {
            Profile.Instance.Data.CurrentBuilding = _currentBInstance;
            UIManager.Instance.UIPanelCity.GetComponentInChildren<UIPanelCity_Buildings>().ToggleShowNewBuildings(false);
            //UIManager.Instance.UIPanelCity.GetComponentInChildren<UIPanelCity_Buildings>().SelectBuilding(_city.Instance.Buildings.Find(x => x.Data is DataTownHall))
            //_city.Build(_currentBInstance);
            //_uigroupBuilding.SelectFirstCell();//Todo: 
        }
        else // Upgrade
        {
            if(_city.Inventory.TryRemoveInventory(_currentBInstance.Data.Price))
                _currentBInstance.Level++;
        }

        UIManager.Instance.UIPanelCity.Refresh();
    }
}

[Serializable]
public class UIBuildingElement
{
    public DataBuilding Data;
    public UIBuilding UIBuilding;
}