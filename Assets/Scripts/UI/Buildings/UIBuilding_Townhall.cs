﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBuilding_TownHall : UIBuilding
{
    [SerializeField] Text _textCurrentBuilding;
    [SerializeField] UIGroupInventory _uiGroupInventory;
    [SerializeField] Button _newBuildingButton;

    void OnEnable()
    {
        _uiGroupInventory.OnCellClicked += OnCellClicked;    
    }

    private void OnDisable()
    {
        _uiGroupInventory.OnCellClicked -= OnCellClicked;
    }

    void OnCellClicked(UICellInventory cell)
    {
        BuildingInstance currentBInstance = Profile.Instance.Data.CurrentBuilding;
        bool isFull = currentBInstance != null && currentBInstance.Inventory.HasItem(cell.ItemInstance.Data, Profile.Instance.Data.CurrentBuilding.Data.Price.ItemList.Find(x => x.Data == cell.ItemInstance.Data).Quantity);
        bool isPaid = _city.Inventory.TryRemoveItem(cell.ItemInstance.Data, 1);
        if (!isFull && isPaid)
        {
            Profile.Instance.Data.CurrentBuilding.Inventory.AddItem(cell.ItemInstance.Data, 1);
            if(currentBInstance.Inventory.HasInventory(currentBInstance.Data.Price))
            {
                _city.BuildNew(currentBInstance);
                UIManager.Instance.UIPanelCity.GetComponentInChildren<UIPanelCity_Buildings>().SwitchSelectedCell(Profile.Instance.Data.CurrentBuilding);
                Profile.Instance.Data.CurrentBuilding = null;
            }
            UIManager.Instance.UIPanelCity.Refresh();
        }
    }

    protected override void OnRefresh()
    {
        if (Profile.Instance.Data.CurrentBuilding == null || Profile.Instance.Data.CurrentBuilding.Data == null)
        {
            _newBuildingButton.gameObject.SetActive(true);
            _textCurrentBuilding.text = "No building in progress";
        }     
        else
        {
            _newBuildingButton.gameObject.SetActive(false);
            _uiGroupInventory.Setup(Profile.Instance.Data.CurrentBuilding.Inventory, Profile.Instance.Data.CurrentBuilding.Data.Price);
            _textCurrentBuilding.text = Profile.Instance.Data.CurrentBuilding.Data.Name;
        }

        
    }
    public void zClick_NewBuilding()
    {
        UIManager.Instance.UIPanelCity.BuildingsPanel.ToggleShowNewBuildings(true);
    }
}
