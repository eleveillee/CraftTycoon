﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBuilding_TrainingGround : UIBuilding
{
    [SerializeField] UIGroupUnits _uiGroupUnits;
    
    protected override void OnSetup()
    {
        _uiGroupUnits.Setup(_city.Instance.Army);
    }

    protected override void OnRefresh()
    {
        _uiGroupUnits.Refresh();
    }
    
    /*public override float GetProgress()
    {
        if (_data == null || m_timeLastCrafting == 0f || _building.GetNumberWorker() <= 0)
            return 0f;

        //Debug.Log("Prod.Progress2 = " + (Time.time - m_timeLastCrafting) + " / " + _data.CoolDown + " = " + Mathf.Clamp((Time.time - _timeLastProduction) / _data.CoolDown, 0f, 1f));
        return Mathf.Clamp((Time.time - m_timeLastCrafting) / _data.CoolDown, 0f, 1f);
    }

    public override Sprite GetVisual()
    {
        return null;
        //return _data == null ? null : _data.ItemOutput.Sprite;
    }*/
}
