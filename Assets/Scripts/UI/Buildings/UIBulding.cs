﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBuilding : MonoBehaviour {

    protected BuildingInstance _bInstance;
    protected City _city;

    protected virtual void OnSetup() { Refresh(); }
    public void Setup(BuildingInstance bInstance, City city)
    {
        _city = city;
        _bInstance = bInstance;
        OnSetup();
        Refresh();
    }

    protected virtual void OnRefresh() { }
    public void Refresh()
    {
        OnRefresh();
    }

}
