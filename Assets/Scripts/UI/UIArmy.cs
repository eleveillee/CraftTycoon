﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIArmy : MonoBehaviour {
    
    public UIGroupUnits UiGroupUnits;

    [SerializeField]
    Text _textArmyName;

    [SerializeField]
    Text _textArmyPower;

    Army _army;
    // Use this for initialization
    public void Setup (Army army) {
        _army = army;
        Refresh();
	}

    public void Refresh()
    {
        _textArmyName.text = _army.Name;
        _textArmyPower.text = _army.Power.ToString();
        UiGroupUnits.Setup(_army);
    }
}
