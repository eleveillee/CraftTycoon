﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICellBuilding : MonoBehaviour {
    
    [SerializeField] Text _textBuildingName;
    [SerializeField] UIGroupInventory _inventoryGroup;
    [SerializeField] Image _imageVisual;
    [SerializeField] Slider _sliderProgress;

    [SerializeField] GameObject _priceGameobject;
    [SerializeField] GameObject _progressGameObject;
    [SerializeField] GameObject _warningInfoGameObject;
    [SerializeField] Text _warningInfoText;

    //[NonSerialized]
    public BuildingInstance BuildingInstance;
    public Action<UICellBuilding> OnCellClick;

     City _city;
    bool _isNewBuilding;

    // Use this for initialization
    public void zClick_Cell () {

        OnCellClick.SafeInvoke(this);
    }

    public void Setup(BuildingInstance buildingInstance, City city, bool isNewBuilding, bool isSelected)
    {
        _isNewBuilding = isNewBuilding;
        _city = city;
        BuildingInstance = buildingInstance;
        _textBuildingName.text = buildingInstance.Data.Name;

        UpdateProgressSection();


        GetComponent<Button>().interactable = !isSelected;


        if (isNewBuilding)
            _inventoryGroup.Setup(buildingInstance.Data.Price);
    }

    void UpdateProgressSection()
    {
        _imageVisual.gameObject.SetActive(!_isNewBuilding);
        _sliderProgress.gameObject.SetActive(!_isNewBuilding);
        if (!_isNewBuilding)
        {
            Sprite visual = BuildingInstance.GetVisual(_city);
            _imageVisual.gameObject.SetActive(visual != null);
            _imageVisual.sprite = visual;

            float progress = BuildingInstance.GetProgress(_city);
            _sliderProgress.gameObject.SetActive(progress > -0.5f);
            _sliderProgress.value = progress;
        }

        bool isRequiringEmployee = !_isNewBuilding && BuildingInstance.Data.DoRequireEmployee && BuildingInstance.GetNumberWorker() == 0;

        _progressGameObject.gameObject.SetActive(!_isNewBuilding && !isRequiringEmployee);
        _warningInfoGameObject.gameObject.SetActive(isRequiringEmployee);
        _priceGameobject.gameObject.SetActive(_isNewBuilding);
    }

    private void Update()
    {
        if (BuildingInstance == null)
            return;

        _sliderProgress.value = BuildingInstance.GetProgress(_city);
    }
}
