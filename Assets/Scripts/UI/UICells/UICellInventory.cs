﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICellInventory : MonoBehaviour {
    
    [SerializeField] Text _textItemName;
    [SerializeField] Text _textItemQuantity;
    [SerializeField] private Image _image;
    public Action<UICellInventory> OnCellClick;
    public ItemInstance ItemInstance;
    // Use this for initialization
    public void zClick_Cell () {
        OnCellClick.SafeInvoke(this);
	}

    public void Setup(ItemInstance itemInstance, ItemInstance comparingItemInstance)
    {
        ItemInstance = itemInstance;
        if(comparingItemInstance != null)
        {
            _textItemQuantity.text = itemInstance.Quantity + "/" + comparingItemInstance.Quantity;
        }
        else
        {
            _textItemQuantity.text = itemInstance.Quantity.ToString();
        }
        _textItemName.text = itemInstance.Data.Name;
        
        _image.sprite = itemInstance.Data.Sprite;
    }
}
