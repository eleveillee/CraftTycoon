﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICellJob : MonoBehaviour {
    
    [SerializeField] Text _textJob;
    [SerializeField] Text _textNumberWorker;
    public Action<UICellJob> OnCellClick;

    City _city;
    JobInstance _job;
    // Use this for initialization
    /*public void zClick_Cell () {
        OnCellClick.SafeInvoke(this);
	}*/

    public void Setup(City city, JobInstance job)
    {
        
        _job = job;
        //Debug.Log("SetupUI : " + _job.Data.Name + "(" + _job.Workers + ")");
        _city = city;
        Refresh();
    }

    void Refresh()
    {
        _textJob.text = _job.Data.Name;
        _textNumberWorker.text = _job.Workers.ToString();
    }

    public void zClick_AddWorker(bool isAdding)
    {
        if(isAdding && _city.Instance.AvailableWorkers > 0)
        {
            _job.Workers++;
        }
        else if (!isAdding && _job.Workers > 0)
        {
            _job.Workers--;
        }

        UIManager.Instance.UIPanelCity.Refresh();
    }
}
