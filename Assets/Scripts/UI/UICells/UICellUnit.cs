﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICellUnit : UICell {
    
    [SerializeField] Text _textUnitName;
    [SerializeField] Text _textQty;
    [SerializeField] Text _textPower;
    [SerializeField] Button _buttonAdd;
    public UnitInstance Instance;
    bool _isSmall;

    public void Setup(UnitInstance unitInstance, bool isSmall)
    {
        _isSmall = isSmall;
        Instance = unitInstance;
        Refresh();
    }

    void Refresh()
    {
        _textPower.text = _isSmall ? Instance.Power.ToString() : Instance.Data.Power.ToString();
        _textUnitName.text = Instance.Data.Name;
        _textQty.text = Instance.Quantity.ToString();

        if(_buttonAdd != null)
        {
            _buttonAdd.gameObject.SetActive(!_isSmall);
            _buttonAdd.interactable = (Profile.Instance.Data.Capital.Inventory.HasInventory(Instance.Data.Price)); // Todo: Use proper city instead of capital
            //_inventoryGroup.Setup(buildingInstance.Data.Price, true);
        }
    }

    public void zClick_AddUnit()
    {
        if (!Profile.Instance.Data.Capital.Inventory.TryRemoveInventory(Instance.Data.Price))
            return;

        Instance.Quantity++;
        UIManager.Instance.UIPanelCity.BuildingsPanel.Refresh();
    }
}
