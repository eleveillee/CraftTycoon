﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class UIGroup : MonoBehaviour
{
    protected List<UICell> _cells = new List<UICell>();
    [SerializeField] UICell _prefabCell;
    public Action<UICell> OnCellClicked;
        
    /*public void Setup(City city, bool isShowingNewBuilds = false)
    {
        _isShowingNewBuilds = isShowingNewBuilds;
        _city = city;
        Refresh();
        SelectFirstCell();
    }*/

    /*void SelectFirstCell()
    {
        if (_cells == null || _cells.Count <= 0)
            return;

        OnCellClick(_cells[0]);
    }*/

    public void Refresh() //Todo: Remove delegates
    {
        for (int i = _cells.Count - 1; i >= 0; i--)
        {
            Destroy(_cells[i].gameObject);
        }
        _cells.Clear();

        /*if (_isShowingNewBuilds) //New Builds
        {
            foreach (BuildingInstance bInstance in _city.Instance.Buildings)
            {
                //BuildingInstance buildingInstance = _city.Instance.Buildings.Find(x => x.Data == dataBuilding);
                //if (buildingInstance == null || buildingInstance.Level > 0 || !dataBuilding.Requirement.IsCompleted(_city))

                if(bInstance.Level > 0 || !bInstance.Data.Requirement.IsCompleted(_city))
                    continue;

                UICellBuilding cellBuilding = Instantiate(_prefabCellBuilding, transform);
                cellBuilding.Setup(bInstance, _city, true, bInstance == CurrentBuildingInstance);
                cellBuilding.OnCellClick += OnCellClick;
                _cells.Add(cellBuilding);
            }
        }
        else // Already Built
        {
            foreach (Building building in _city.Buildings)
            {
                UICellBuilding cellBuilding = Instantiate(_prefabCellBuilding, transform);
                cellBuilding.Setup(building.Instance,_city, false, building.Instance == CurrentBuildingInstance);
                cellBuilding.OnCellClick += OnCellClick;
                _cells.Add(cellBuilding);
            }
        }*/
        OnRefresh();
    }

    public UICell AddCell()
    {
        UICell cell = Instantiate(_prefabCell, transform);
        //cellBuilding.Setup(bInstance, _city, true, bInstance == CurrentBuildingInstance);
        cell.OnCellClick += OnCellClick;
        _cells.Add(cell);
        return cell;
    }

    protected virtual void OnRefresh() { }

    private void OnCellClick(UICell cell)
    {
        //CurrentBuildingInstance = cell.BuildingInstance;
        OnCellClicked.SafeInvoke(cell);
    }
}
