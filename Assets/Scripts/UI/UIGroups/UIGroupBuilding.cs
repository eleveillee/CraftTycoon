﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class UIGroupBuilding : MonoBehaviour
{
    List<UICellBuilding> _cells = new List<UICellBuilding>();
    [SerializeField] UICellBuilding _prefabCellBuilding;
    public Action<UICellBuilding> OnCellClicked;
    public bool _isShowingNewBuilds;

    BuildingInstance CurrentBuildingInstance;

    private City _city;
    public void Setup(City city, bool isShowingNewBuilds = false)
    {
        _isShowingNewBuilds = isShowingNewBuilds;
        _city = city;
        Refresh();
        SelectFirstCell();
    }

    void SelectFirstCell()
    {
        if (_cells == null || _cells.Count <= 0)
            return;

        OnCellClick(_cells[0]);
    }

    public void Refresh() //Todo: Remove delegates
    {
        for (int i = _cells.Count - 1; i >= 0; i--)
        {
            Destroy(_cells[i].gameObject);
        }
        _cells.Clear();

        List<BuildingInstance> orderedBuildings = new List<BuildingInstance>(_city.Instance.Buildings);
        for (int i = GameDef.Instance.Data.BuildingsPriorityOrder.Count - 1; i >= 0; i--)
        {
            int index = orderedBuildings.FindIndex(x => x.Data == GameDef.Instance.Data.BuildingsPriorityOrder[i]);
            if (index == -1)
                continue;
            
            orderedBuildings.MoveItemAtIndexToFront(index);
        }
        
        foreach (BuildingInstance bInstance in orderedBuildings)
        {
            bool testNewBuildsContinue = _isShowingNewBuilds && (bInstance.Level > 0 || !bInstance.Data.Requirement.IsCompleted(_city));
            bool testBuildsContinue = !_isShowingNewBuilds && bInstance.Level <= 0;
            if (testNewBuildsContinue || testBuildsContinue)
                continue;
            
            UICellBuilding cellBuilding = Instantiate(_prefabCellBuilding, transform);
            cellBuilding.Setup(bInstance, _city, _isShowingNewBuilds, bInstance == CurrentBuildingInstance);
            cellBuilding.OnCellClick += OnCellClick;
            _cells.Add(cellBuilding);
        }
    }

    private void OnCellClick(UICellBuilding cell)
    {
        CurrentBuildingInstance = cell.BuildingInstance;
        OnCellClicked.SafeInvoke(cell);
    }
}
