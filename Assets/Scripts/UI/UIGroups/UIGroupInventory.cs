﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class UIGroupInventory : MonoBehaviour
{
    List<UICellInventory>_cells = new List<UICellInventory>();
    [SerializeField] UICellInventory _prefabCellInventory;
    public Action<UICellInventory> OnCellClicked;

    private Inventory _inventory;
    private Inventory _comparingInventory;
    public void Setup(Inventory inventory, Inventory comparingInventory = null)
    {
        _inventory = inventory;
        _comparingInventory = comparingInventory;
        Refresh();
        inventory.OnItemAdded += Refresh;
    }

    public void Refresh()
    {
        for (int i = _cells.Count - 1; i >= 0; i--)
        {
            Destroy(_cells[i].gameObject);
        }
        _cells.Clear();

        if(_inventory == null)
        {
            return;
        }
        
        foreach (ItemInstance itemInstance in _inventory.ItemList)
        {
            ItemInstance comparingItemInstance = null;
            if(_comparingInventory != null)
                comparingItemInstance = _comparingInventory.ItemList.Find(x => x.Data == itemInstance.Data);

            bool isNotComparingInventoryContinue = _comparingInventory == null && itemInstance.Quantity <= 0;
            bool iscomparingInventoryContinue = _comparingInventory != null && (comparingItemInstance == null || comparingItemInstance.Quantity <= 0);
            //Debug.Log(isNotComparingInventoryContinue + " = " + (_comparingInventory == null) + " && " + (itemInstance.Quantity <= 0));

            if (isNotComparingInventoryContinue || iscomparingInventoryContinue)
                continue;

            UICellInventory cellInventory = Instantiate(_prefabCellInventory, transform);
            cellInventory.Setup(itemInstance, comparingItemInstance);
            cellInventory.OnCellClick += OnCellClick;
            _cells.Add(cellInventory);
        }
    }

    private void OnCellClick(UICellInventory cell)
    {
        OnCellClicked.SafeInvoke(cell);
    }
}
