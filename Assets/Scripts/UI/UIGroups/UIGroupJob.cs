﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class UIGroupJob : MonoBehaviour
{
    List<UICellJob>_cells = new List<UICellJob>();
    [SerializeField] UICellJob _prefabCellJob;
    public Action<UICellJob> OnCellClicked;

    City _city;
    public void Setup(City city)
    {
        _city = city;
        Refresh();
        //inventory.OnItemAdded += Refresh;
    }

    public void Refresh()
    {
        for (int i = _cells.Count - 1; i >= 0; i--)
        {
            Destroy(_cells[i].gameObject);
        }
        _cells.Clear();

        if(_city.Instance.Jobs == null)
        {
            return;
        }

        //foreach (JobInstance job in _city.Instance.Jobs)
        foreach (DataJob dataJob in GameDef.Instance.Data.Jobs)
        {
            //Debug.Log("Looking for : " + dataJob.Name + " in " + _city.Instance.Jobs.Count);
            JobInstance jobInstance = _city.Instance.Jobs.Find(x => x.Data == dataJob);
            if(jobInstance == null)
            {
                Debug.LogWarning("[UIGroupJob] Unknown JobInstance for - " + dataJob);
                continue;
            }

            //Debug.Log("Testing : " + jobInstance.Data.Name);
            if (!dataJob.Requirement.IsCompleted(_city))
                continue;

            //Debug.Log("JobPass : " + jobInstance.Data.Name);

            UICellJob cell = Instantiate(_prefabCellJob, transform);
            cell.Setup(_city, jobInstance);
            cell.OnCellClick += OnCellClick;
            _cells.Add(cell);
        }
    }

    private void OnCellClick(UICellJob cell)
    {
        OnCellClicked.SafeInvoke(cell);
    }
}
