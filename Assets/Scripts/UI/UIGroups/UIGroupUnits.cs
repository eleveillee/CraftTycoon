﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class UIGroupUnits : UIGroup
{
    [SerializeField]
    private bool _isSmall;

    private Army _army;
    
    public void Setup(Army army)
    {
        _army = army;
        Refresh();
    }

    protected override void OnRefresh()
    {
        foreach (UnitInstance instance in _army.Units)
        {
            if (_isSmall && instance.Quantity < 1)
                continue;

            UICellUnit cell = (UICellUnit)AddCell();
            cell.Setup(instance, _isSmall);
        }
    }
}
