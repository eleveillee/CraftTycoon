﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInventoryWidget : MonoBehaviour {

    [SerializeField]
    private UIGroupInventory _uiInventoryGroup;

    Inventory _inventory;

    protected void OnEnable()
    {
        _uiInventoryGroup.Setup(_inventory);

        //GameEvents.OnTrainDocked += (city) => Refresh();  //Need to remove OnClose()
        //GameEvents.OnTrainLeave += (cityFrom, cityTo) => Refresh(); //Need to remove OnClose()

        //_trainInventoryGroup.OnCellClicked += OnTrainCellCicked;

    }

    public void Setup(Inventory inventory)
    {
        _inventory = inventory;
    }

}

