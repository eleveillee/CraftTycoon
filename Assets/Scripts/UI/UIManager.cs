﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    [Header("UI Panels and overlays")]

    public UIOverlay UIOverlay;
    public UIPanelPause UIPanelPause;
    public UIPanelCity UIPanelCity;
    public UIPanelEvent UIPanelEvent;
    public UIPanelArmy UIPanelArmy;
    public UIPanelResearch UIPanelResearch;

    //public UIPanelFightResult UIPanelFightResult;
    
    [NonSerialized]
    public UIPanel CurrentPanel;
    
    // Use this for initialization
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        CloseAllPanels();
    }   

    // Panels
    public void CloseAllPanels()
    {
        UIPanelPause.Close();
        UIPanelCity.Close();
        UIPanelEvent.Close();
        UIPanelResearch.Close();
        UIPanelArmy.Close();
    }
    
    // Overlays
    public void ShowUIOVerlay(bool isVisible = true)
    {
        UIOverlay.Show(isVisible);
    }
}