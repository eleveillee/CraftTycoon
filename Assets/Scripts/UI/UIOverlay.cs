﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIOverlay : MonoBehaviour
{
	[SerializeField]
	private Text _goldText;
	
	void Awake () 
	{
        Profile.Instance.Data.Inventory.Currency.OnUpdate += OnUpdateCurrency;
    }

    private void OnDestroy()
    {
        Profile.Instance.Data.Inventory.Currency.OnUpdate -= OnUpdateCurrency;
    }

    void OnUpdateCurrency(Currency currency)
    {
        SetGold(currency.Gold);
    }

	public void SetGold(int value)
	{
		_goldText.text = value.ToString();
	}

    public void Show(bool isVisible)
    {
        gameObject.SetActive(isVisible);
    }
    
    public void OnClick_Research()
    {
        UIManager.Instance.UIPanelResearch.Open();
    }
}
