﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class UIPanel : MonoBehaviour
{
    protected bool _isOpen;
    public void Close()
    {
        UIManager.Instance.CurrentPanel = null;

        gameObject.SetActive(false);

        if (!_isOpen)
            return;

        _isOpen = false;
        OnClose();
    }

    public void Open(bool isCloseAll = true)
    {
        if (isCloseAll)
        {
            UIManager.Instance.CloseAllPanels();
        }

        UIManager.Instance.CurrentPanel = this;

        gameObject.SetActive(true);

        if (_isOpen)
            return;

        _isOpen = true;
        OnOpen();
    }

    protected virtual void OnOpen() { }
    protected virtual void OnClose() { }
}
