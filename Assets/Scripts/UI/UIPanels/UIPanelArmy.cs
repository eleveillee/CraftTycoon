﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelArmy : UIPanel
{
    [SerializeField] UIArmy _cityUIArmy;
    [SerializeField] UIArmy _newUIArmy;
    [SerializeField] UIArmy _enemyUIArmy;

    Army _newArmy;
    City _city;
    EventMarker _eventMarker;

    public UIPanelArmy Setup(EventMarker eventMarker, City city)
    {
        _newArmy = new Army();
        _city = city;
        _eventMarker = eventMarker;
        _cityUIArmy.Setup(_city.Instance.Army);
        _enemyUIArmy.Setup(_eventMarker.EventFight.Army);
        _newUIArmy.Setup(_newArmy);

        return this;
    }

    void OnEnable()
    {
        _newUIArmy.UiGroupUnits.OnCellClicked += OnNewArmyUnitClicked;
        _cityUIArmy.UiGroupUnits.OnCellClicked += OnCityUnitClicked;
    }

    void OnDisable()
    {
        _newUIArmy.UiGroupUnits.OnCellClicked -= OnNewArmyUnitClicked;
        _cityUIArmy.UiGroupUnits.OnCellClicked -= OnCityUnitClicked;
    }

    private void OnNewArmyUnitClicked(UICell cell)
    {
        UnitInstance instanceClicked = ((UICellUnit)cell).Instance;
        UnitInstance cityArmyInstance = _city.Instance.Army.Units.Find(x => x.Data == instanceClicked.Data);
        cityArmyInstance.Quantity++;
        instanceClicked.Quantity--;
        Refresh();
    }

    private void OnCityUnitClicked(UICell cell)
    {
        UnitInstance instanceClicked = ((UICellUnit)cell).Instance;
        UnitInstance newArmyInstance =  _newArmy.Units.Find(x => x.Data == instanceClicked.Data);
        newArmyInstance.Quantity++;
        instanceClicked.Quantity--;
        Refresh();
    }

    public void Refresh()
    {
        _newUIArmy.Refresh();
        _cityUIArmy.Refresh();
        //_enemyUIArmy.Refresh();
    }

    public void zClick_SendArmy()
    {
        if (_newArmy.Power <= 0)
            return;

        ArmyManager.Instance.LaunchConvoy(_city, _eventMarker, _newArmy);
        Close();
    }
}