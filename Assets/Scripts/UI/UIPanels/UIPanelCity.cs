﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelCity : UIPanel
{
    [SerializeField] Text _textName;
    [SerializeField] Text _textPrice;
    [SerializeField] GameObject _lockedPanel;
    [SerializeField] GameObject _unlockedPanel;

    public UIPanelCity_Buildings BuildingsPanel;
    public UIPanelCity_Inventory InventoryPanel;
    public UIPanelCity_Workers WorkersPanel;
    [SerializeField] PanelCitySub _currentPanelCitySub;

    [Header("Buttons")]
    [SerializeField] Button _buttonBuilding;
    [SerializeField] Button _buttonInventory;
    [SerializeField] Button _buttonWorkers;

    [SerializeField] ColorBlock _normalColors;
    [SerializeField] ColorBlock _selectedColors;

    City _city;

    public UIPanel Setup(City city)
    {
        _city = city;
        WorkersPanel.Setup(_city);
        InventoryPanel.Setup(_city);
        BuildingsPanel.Setup(_city);
        _textName.text = _city.Instance.Data.Name.ToString();
        _textPrice.text = "100";//CityManager.Instance.GetCityPrice(_city.Instance.Data.Name).ToString();
        _currentPanelCitySub = PanelCitySub.Building;
        ToggleUnlockPanel();
        return this;
    }

    private void ToggleUnlockPanel()
    {
        _lockedPanel.SetActive(!_city.Instance.IsUnlocked);
        _unlockedPanel.SetActive(_city.Instance.IsUnlocked);
        if(_city.Instance.IsUnlocked)
        {
            SwitchSubPanel(_currentPanelCitySub);
        }
        Refresh();
    }

    public void Refresh()
    {
        //_inventoryPanel.Refresh();
        _buttonWorkers.GetComponentInChildren<Text>().text = _city.Instance.AvailableWorkers + "/" + _city.Instance.MaxWorkers + "/" + _city.Instance.AvailableHousing + " W";
        BuildingsPanel.Refresh();
        if (_currentPanelCitySub == PanelCitySub.Workers) { WorkersPanel.Refresh(); }
    }

    private void Update()
    {
        if (!_isOpen)
            return;

        DoCheat();

        InventoryPanel.Refresh();
    }

    void DoCheat()
    {
        if(Input.GetKeyDown(KeyCode.F1))
        {
            _city.Inventory.ItemList.ForEach(x => x.Quantity = _city.Inventory.MaxSize); //FillInventory Cheat
        }
        else if (Input.GetKeyDown(KeyCode.F2))
        {
            foreach (BuildingInstance instance in _city.Instance.Buildings)
            {
                _city.BuildNew(instance); // BuildAll Cheat
                Refresh();
            }
        }
        else if (Input.GetKeyDown(KeyCode.F3))
        {
            foreach (UnitInstance instance in _city.Instance.Army.Units) // BuildArmy Cheat
            {
                instance.Quantity = 100;
                Refresh();
            }
        }
    }

    public void zClick_Buildings()
    {
        SwitchSubPanel(PanelCitySub.Building);
    }

    public void zClick_Workers()
    {
        if(_currentPanelCitySub == PanelCitySub.Building)
        {
            SwitchSubPanel(PanelCitySub.Workers);
        }
        else
        {
            SwitchSubPanel(PanelCitySub.Building);
        }
    }

    public void SwitchSubPanel(PanelCitySub sub)
    {
        _currentPanelCitySub = sub;
        BuildingsPanel.gameObject.SetActive(false);
        WorkersPanel.gameObject.SetActive(false);

        _buttonBuilding.colors = _normalColors;
        _buttonInventory.colors = _normalColors;
        _buttonWorkers.colors = _normalColors;

        if (sub == PanelCitySub.Building)
        {
            _buttonBuilding.colors = _selectedColors;
            BuildingsPanel.gameObject.SetActive(true);
            BuildingsPanel.Refresh();
        }
        else if (sub == PanelCitySub.Workers)
        {
            _buttonWorkers.colors = _selectedColors;
            WorkersPanel.gameObject.SetActive(true);
            WorkersPanel.Refresh();
        }
    }

    public void zClick_Unlock()
    {
        if(true)
        {
            _city.Instance.State = CityState.Unlocked;
            ToggleUnlockPanel();
        }
    }
}

public enum PanelCitySub
{
    Building,
    Inventory,
    Workers
}