﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIPanelCity_Buildings : MonoBehaviour
{
    [SerializeField] UIGroupBuilding _groupBuildings;   
    [SerializeField] Button _newBuildingButton;
    [SerializeField] UIBuildingSubsection _uiBuildingSubsection;

    [SerializeField] Button _backButton;
    City _city;

    public void Setup(City city)
    {
        _groupBuildings.OnCellClicked += OnCellClicked;
        _city = city;
        ToggleShowNewBuildings(false);
        //_uiBuildingSubsection.Setup(_city.Buildings[0].Instance);
    }
    
    public void SwitchSelectedCell(BuildingInstance bInstance)
    {
        _uiBuildingSubsection.Setup(bInstance, _city);
        Refresh();
    }
    
    public void ToggleShowNewBuildings(bool isNewBuilds)
    {
        _backButton.gameObject.SetActive(isNewBuilds);
        _groupBuildings.Setup(_city, isNewBuilds);
        Refresh();
    }
    
    public void OnCellClicked(UICellBuilding cell)
    {
        SwitchSelectedCell(cell.BuildingInstance);
    }
    
    public void Refresh()
    {
         _groupBuildings.Refresh();
        _uiBuildingSubsection.Refresh();
    }

    public void SelectBuilding(BuildingInstance bInstance)
    {
        SwitchSelectedCell(bInstance);
    }


    public void zClick_NewBuildingBack()
    {
        ToggleShowNewBuildings(false);
    }
}
