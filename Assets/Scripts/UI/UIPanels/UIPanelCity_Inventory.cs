﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelCity_Inventory : MonoBehaviour
{
    [SerializeField] UIGroupInventory _cityInventoryGroup;
    [SerializeField] UIGroupInventory _trainInventoryGroup;
    [SerializeField] GameObject _trainInventory;

    City _city;

    protected void OnEnable()
    {
        Refresh();
    }

    public void Refresh()
    {
        if (_city == null)
            return;
            
        _cityInventoryGroup.Refresh();
    }

    public void Setup(City city)
    {
        _city = city;
        _cityInventoryGroup.Setup(_city.Instance.Inventory);
    }
}