﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelCity_Workers : MonoBehaviour
{
    [SerializeField]
    Text _workerLeft;

    [SerializeField]
    UIGroupJob _uiGroupJob;

    [SerializeField]
    Button _addWorkerButton;

    City _city;

    public void Setup(City city)
    {
        _city = city;
        _uiGroupJob.Setup(_city);
        Refresh();
    }

    public void Refresh()
    {
        _workerLeft.text = _city.Instance.AvailableWorkers.ToString();
        _uiGroupJob.Refresh(); // Second refresh because of setup    
    }

    private void Update()
    {
        if (_city.Instance.MaxWorkers >= _city.Instance.AvailableHousing || !_city.Instance.Inventory.HasInventory(GameDef.Instance.Data.NewWorkerCost))
        {
            _addWorkerButton.interactable = false;
        }
        else
        {
            _addWorkerButton.interactable = true;
        }
    }

    public void zClick_AddMaxWorker()
    {
        //Debug.Log(_city.Instance.MaxWorkers + " >= " + _city.Instance.AvailableHousing + " = " + (_city.Instance.Inventory.HasInventory(GameDef.Instance.Data.NewWorkerCost)));

        if (_city.Instance.MaxWorkers >= _city.Instance.AvailableHousing || !_city.Instance.Inventory.TryRemoveInventory(GameDef.Instance.Data.NewWorkerCost))
            return;

        _city.Instance.MaxWorkers++;
        UIManager.Instance.UIPanelCity.Refresh();
    }

    public void zClick_BackButton()
    {
        UIManager.Instance.UIPanelCity.SwitchSubPanel(PanelCitySub.Building);
    }
}