﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelEvent : UIPanel
{
    [SerializeField] Text _textEventName;
    [SerializeField] Text _textEventDescription;

    [SerializeField] UIPanelEvent_Prefight _panelPrefight;
    [SerializeField] UIPanelEvent_Result _panelResult;

    Army _playerArmy;
    EventMarker _eventMarker;

    public UIPanel Setup(Army playerArmy, EventMarker eventMarker)
    {
        _playerArmy = playerArmy;
        _eventMarker = eventMarker;
        SetupCommon();
        if(eventMarker.EventFight.DockedConvoy == null) //todo deal with coming convoys
        {
            _panelResult.gameObject.SetActive(false);
            _panelPrefight.gameObject.SetActive(true);
            _panelPrefight.Setup(_eventMarker);
        }
        else
        {
            _panelPrefight.gameObject.SetActive(false);
            _panelResult.gameObject.SetActive(true);
            _panelResult.Setup(_eventMarker, _playerArmy);

        }

        Refresh();
        return this;
    }

    void SetupCommon()
    {
        _textEventName.text = _eventMarker.EventFight.Name;
        _textEventName.text = _eventMarker.EventFight.Description;
    }

    public void Refresh()
    {
        SetupCommon();
    }
}