﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelEvent_Prefight : MonoBehaviour
{ 
    [SerializeField] UIArmy _enemyUIArmy;
    
    EventMarker _eventMarker;

    public void Setup(EventMarker eventMarker)
    {
        _eventMarker = eventMarker;
        _enemyUIArmy.Setup(_eventMarker.EventFight.Army);
        Refresh();
    }

    public void Refresh()
    {
        _enemyUIArmy.Refresh();
    }

    public void zClick_SendArmy()
    {
         UIManager.Instance.UIPanelEvent.Close();
        ArmyManager.Instance.PrepareArmyTowardsEvent(_eventMarker);
    }
}