﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelEvent_Result : MonoBehaviour
{
    [SerializeField] Text _textWinner;

    [SerializeField] GameObject _uiLootInventoryGO;
    [SerializeField] UIGroupInventory _uiLootInventory;

    Army _playerArmy;
    EventMarker _eventMarker;

    public void Setup(EventMarker eventMarker, Army playerArmy)
    {
        _eventMarker = eventMarker;
        _playerArmy = playerArmy;
        _textWinner.gameObject.SetActive(true);
        
        if(_eventMarker.EventFight.Result == null)
        {
            _eventMarker.EventFight.Fight(_playerArmy);
        }
      
    
        _uiLootInventoryGO.SetActive(true); // ToRemove
        _uiLootInventory.Setup(_eventMarker.EventFight.Loot);

        //_playerArmy = playerArmy;
        //_playerUIArmy.Setup(_playerArmy);

        //_enemyUIArmy.Setup(_eventMarker.EventFight.Army);
        //Refresh();
    }

    public void zClick_ReturnArmy()
    {
        UIManager.Instance.UIPanelEvent.Close();
        EventManager.Instance.ClearEvent(_eventMarker.EventFight);

        //_eventMarker.EventFight.DockedConvoy. //ToDo Send convoy back. Need to update Source/Destination
    }
}

public class FightResult
{
    public bool DidPlayerWin;
}