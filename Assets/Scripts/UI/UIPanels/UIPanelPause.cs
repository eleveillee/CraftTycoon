﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelPause : UIPanel
{
    protected override void OnOpen()
    {
    }

    protected override void OnClose()
    {
    }

    void Update()
    {
        if (!_isOpen) 
            return;
    }

    public void OnQuit_Button()
    {
        //GameManager.Instance.FSMGameState.ChangeState(new GameState_MainMenu());
    }

    public void OnDebug_Button()
    {
        //UIManager.Instance.ShowPanel(PanelType.Debug);
    }
}