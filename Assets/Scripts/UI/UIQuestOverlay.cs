using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIQuestOverlay : MonoBehaviour {

    [SerializeField] Text _textQuest;

    private void Start()
    {
        QuestManager.Instance.OnNewQuest += OnNewQuest;
        Refresh();
    }

    void Refresh()
    {
        _textQuest.text = "- " + Profile.Instance.Data.MainQuest.Name;
    }

    private void OnNewQuest()
    {
        Refresh();
    }
}
