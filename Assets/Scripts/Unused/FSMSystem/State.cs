﻿using System;
using UnityEngine;
using System.Collections;

namespace FSM
{
    public abstract class State
    {
        protected FSM _fsm;
        public virtual bool IsInstant { get { return true; } }

        public bool IsInitialized;

        public void Initialize(FSM fsm)
        {
            _fsm = fsm;
        }

        public void Enter() { OnEnter(); IsInitialized = true; }
        protected virtual void OnEnter() { }

        public void Update() { OnUpdate(); }
        protected virtual void OnUpdate() { }
        
        public void Exit() { OnExit(); }
        protected virtual void OnExit() { }
    }
}


